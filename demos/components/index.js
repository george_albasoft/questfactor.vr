import Puzzle0 from './puzzle0'
import Puzzle1 from './puzzle1'
import Puzzle2 from './puzzle2'
import Puzzle3 from './puzzle3'

export default {
  Puzzle0, Puzzle1, Puzzle2, Puzzle3
}