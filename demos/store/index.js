import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // put variables and collections here
    playerName: 'Player',
    panoState: 0,
    playerInventory: [
        {
            key: 'slot1',
            object: null,
        },
        {
            key: 'slot2',
            object: null,
        },
        {
            key: 'slot3',
            object: null,
        }
    ],
    hackingPanel: {
            active: true,
            solved: false
        },
    switchBoard:   {
            active: false,
            solved: false
        }
  },
  mutations: {
    // put sychronous functions for changing state e.g. add, edit, delete
    panoNextState(state) {
        state.panoState++
      },
    
    hackingPanelState(state, status) {
        state.hackingPanel.active = status[0]
        state.hackingPanel.solved = status[1]
      }
  },
  actions: {
    // put asynchronous functions that can call one or more mutation functions
  },
  getters: {
    getPanoState: state => state.panoState
  }
})