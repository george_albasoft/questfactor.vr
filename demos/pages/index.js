import DemoPano from './demo-pano'
import DemoCubePano from './demo-cube-pano'
import DemoVideoPano from './demo-video-pano'
import DemoScene from './demo-scene'
import DemoTour from './demo-tour'
import BankHeist from './bank-heist'
import HackingPanel from './hacking-panel'

const pages = [
  DemoPano,
  DemoCubePano,
  DemoVideoPano,
  DemoScene,
  DemoTour,
  BankHeist,
  HackingPanel
]

export { pages }
