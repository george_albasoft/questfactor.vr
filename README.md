# Vue VR
A Wrapper of [Panolens](https://pchen66.github.io/Panolens/) for building VR applications with Vue
based on [threejs](https://threejs.org/)

QUEST FACTOR - VR

Features:

- VR ready & cross platform (web / mobile)
- scenes recorded with real actors in real world environment to provide full immersion via photo panaramic expereince
- interactive components such as dialog choices and puzzles change scene based on user action
- linear game-play for single players that later could be updated to support multi-player online quest



## Getting started
using npm
```
npm install vuejs-vr --save


## Installing & Running Locally

Clone the repository using git:
```
git clone https://github.com/mudin/vue-vr.git 
```
Installing all dependencies:
```
npm install 
```
Build by webpack:
```
npm run-script build 
```
Run locally:
```
npm start 
```
This will start development server on localhost:8080

## Usage